 /*
    Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using SDL;

bool disassemble;
string filename;
unowned SDL.Screen screen;
bool keys[16];
uint8 *pixels;
bool running;

void main (string[] args) 
{
    try {
        parseArguments(args[1:args.length]);
    } catch (IOError e) {
        stderr.printf ("ERROR: %s\n", e.message);
        return;
    }
    
    File file = File.new_for_path(filename);
    
    if (!file.query_exists()) {
        stderr.printf ("ERROR: File '%s' doesn't exist.\n", file.get_path());
        return;
    }
    
    Program program = new Program(file);
    
    if (disassemble) {
        disassembler(program);
    } else {
        if (!initVideo())
            return;
        
        Memory memory = new Memory(4096);
        Display display = new Display();
        Cpu cpu = new Cpu(memory, display);
        
        memory.loadProgram(program);
    
        running = true;
        
        while (running) {
            handleEvents();
        
            try {
                cpu.cycle();
            } catch (CallStackError e) {
                stderr.printf("ERROR: %s\n", e.message);
            }
            
            for (int i = 0; i < 320; ++i) {
                for (int j = 0; j < 640; ++j) {
                    uint8 *ptr = pixels + (i * 640) + j;
                    *ptr = display.pixels[i/10, j/10] * 255;
                }
            }
                
            screen.flip();
        }
        
        SDL.quit();
    }
}

public void handleEvents ()
{
    Event event;
    
    while (Event.poll (out event) == 1) {
        switch (event.type) {
            case EventType.QUIT:
                running = false;
                break;
            case EventType.KEYDOWN:
                setKey(event.key.keysym.sym, true);
                break;
            case EventType.KEYUP:
                setKey(event.key.keysym.sym, false);
                break;
        }
    }
}

void setKey(uint8 key, bool state)
{
    /*
       CHIP-8     PC
       -------    -------
       1 2 3 C    1 2 3 4
       4 5 6 D    q w e r
       7 8 9 E    a s d f
       A 0 B F    z x c v
    */
    switch (key) {
        case 49:
            keys[0x1] = state;
            break;
        case 50:
            keys[0x2] = state;
            break;
        case 51:
            keys[0x3] = state;
            break;
        case 52:
            keys[0xC] = state;
            break;
            
        case KeySymbol.q:
            keys[0x4] = state;
            break;
        case KeySymbol.w:
            keys[0x5] = state;
            break;
        case KeySymbol.e:
            keys[0x6] = state;
            break;
        case KeySymbol.r:
            keys[0xD] = state;
            break;
            
        case KeySymbol.a:
            keys[0x7] = state;
            break;
        case KeySymbol.s:
            keys[0x8] = state;
            break;
        case KeySymbol.d:
            keys[0x9] = state;
            break;
        case KeySymbol.f:
            keys[0xE] = state;
            break;
            
        case KeySymbol.z:
            keys[0xA] = state;
            break;
        case KeySymbol.x:
            keys[0x0] = state;
            break;
        case KeySymbol.c:
            keys[0xB] = state;
            break;
        case KeySymbol.v:
            keys[0xF] = state;
            break;
    }
}
    
public bool getKey (uint key)
{
    return keys[key];
}
    
public uint8 waitForAnyKeys ()
{
    while (true) {
        for (uint8 i = 0; i < keys.length; ++i) {
            if (keys[i])
                return i;
        }
            
        handleEvents();
    }
}

bool initVideo ()
{
    SDL.init(InitFlag.VIDEO | InitFlag.AUDIO);
    screen = Screen.set_video_mode (640, 320, 8, SurfaceFlag.DOUBLEBUF);
        
    if (null == screen) {
        stderr.printf("ERROR: Could not set video mode.\n");
        return false;
    }
        
    SDL.WindowManager.set_caption("CHIP-8", "");
    
    pixels = screen.pixels;
    
    return true;
}

void parseArguments (string[] args) throws IOError
{
    foreach (string arg in args) {
        if ("--d" == arg) {
            disassemble = true;
            continue;
        }
        
        filename = arg;
    }
    
    if (null == filename)
        throw new IOError.INVALID_ARGUMENT("No program passed.");
}
