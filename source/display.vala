/*
    Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using SDL;

public class Display
{
    public uint8[,] pixels;
    
    public Display ()
    {
        pixels = new uint8[32, 64];
    }
    
    public uint8 draw (uint8[] spr, uint8 x, uint8 y)
    {
        uint8 col = 0;
        
        for (uint8 i = 0; i < spr.length; ++i) {
            uint8 yCorrected = (y + i) % 32;
            
            for (uint8 j = 0; j < 8; ++j) {
                uint8 xCorrected = (x + j) % 64;
                uint8 sprPixel = (spr[i] >> (7 - j)) & 1;
                col |= sprPixel & pixels[yCorrected, xCorrected];
                pixels[yCorrected, xCorrected] ^= sprPixel;
            }
        }
        
        return col;
    }
    
    public void clear ()
    {
        for (uint8 i = 0; i < 32; ++i)
            for (uint8 j = 0; j < 64; ++j)
                pixels[i, j] = 0;
    }
}