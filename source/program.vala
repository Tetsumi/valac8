/*
    Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Store a program into an array.
 * Instructions are converted to little endianness.
 */
public class Program : Object
{
    public uint16[] instructions;
    
    public Program (File file)
    {
        try {
            var dis = new DataInputStream(file.read());
            
            dis.seek(0, SeekType.END);
            instructions = new uint16[dis.tell()/2];
            dis.seek(0, SeekType.SET);
            
            for (int i = 0; i < instructions.length; ++i) {
                uint16 ins = dis.read_uint16();
                //ins = (ins >> 8) | (ins << 8);
                instructions[i] = ins;
            }
        } catch (Error e) {
            stderr.printf ("ERROR: %s\n", e.message);
        }
    }
}
