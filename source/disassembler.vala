/*
    Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public void disassembler (Program program)
{
    for (int i = 0; i < program.instructions.length; ++i) {
        uint16 ins = program.instructions[i];
        stdout.printf("%05x: %04x | %s\n", (0x200)+(i * 2), ins, insToString(ins));
    }
}


public string insToString (uint16 ins)
{
    uint16 op = ins >> 12;
    uint16 args = ins & 0xfff;
    uint16 arg1 = (ins >> 8) & 0xF;
    uint16 arg2 = (ins >> 4) & 0xF;
    uint16 arg3 = ins & 0xF;
    uint16 arg23 = ins & 0xFF;
    
    switch (op) {
        case 0x0:
            if (0x0 == arg1 && 0xE == arg2) {
                if (0xE == arg3)
                    return "RETURN";
                else if(0x0 == arg3)
                    return "CLEAR";
            } else {
                return "%-7s %04Xh".printf("SYS", args);
            }
            break;
        case 0x1:
            return "%-7s %04Xh".printf("JUMP", args);
        case 0x2:
            return "%-7s %04Xh".printf("CALL", args);
        case 0x3:
            return "%-7s V%X, %3d".printf("SKIPEQ", arg1, arg23);
        case 0x4:
            return "%-7s V%X, %3d".printf("SKIPNEQ", arg1, arg23);
        case 0x5:
            if (0x0 == arg3)
                return "%-7s V%X, V%X".printf("SKIPEQ", arg1, arg2);
            break;
        case 0x6:
            return "%-7s V%X, %3d".printf("LOAD", arg1, arg23);
        case 0x7:
            return "%-7s V%X, %3d".printf("ADD", arg1, arg23);
        case 0x8:
            const string caps[] = {
                "LOAD", "OR", "AND",  "XOR", 
                "ADD", "SUB", "SHR", "SUBN", 
                null,   null,  null,   null, 
                null,   null, "SHL"   
            };
            
            return "%-7s V%X, V%X".printf(caps[arg3], arg1, arg2);
        case 0x9:
            if (0x0 == arg3)
                return "%-7s V%X, V%X".printf("SKIPNEQ", arg1, arg2);
            break;
        case 0xA:
            return "%-7s  I, %04Xh".printf("LOAD", args);
        case 0xB:
            return "%-7s V0, %04Xh".printf("JUMP", args);
        case 0xC:
            return "%-7s V%X, %3d".printf("RAND", arg1, arg23);
        case 0xD:
            return "%-7s V%X, V%X, %3d".printf("DRAW", arg1, arg2, arg3);
        case 0xE:
            if (0x9E == arg23)
                return "%-7s V%X".printf("SKIPKEY", arg1);
            else if(0xA1 == arg23)
                return "%-7s V%X".printf("SKIPNKEY", arg1);
            break;
        case 0xF:
            switch (arg23) {
                case 0x07:
                    return "%-7s V%X, DT".printf("LOAD", arg1);
                case 0x0A:
                    return "%-7s V%X".printf("LOADKEY", arg1);
                case 0x15:
                    return "%-7s DT, V%X".printf("LOAD", arg1);
                case 0x18:
                    return "%-7s ST, V%X".printf("LOAD", arg1);
                case 0x1E:
                    return "%-7s  I, V%X".printf("ADD", arg1);
                case 0x29:
                    return "%-7s  I, V%X".printf("LOADSPR", arg1);
                case 0x33:
                    return "%-7s  I, V%X".printf("LOADBCD", arg1);
                case 0x55:
                    return "%-7s  I, V%X".printf("LOADREG", arg1); 
                case 0x65:
                    return "%-7s V%X,  I".printf("LOADREG", arg1);
            }
            break;
    }
    
    return "-- UNKNOWN --";
}