/*
    Copyright 2015 Tetsumi <tetsumi@vmail.me>
  
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

public class Cpu : Object
{
    uint8[]   regV;
    uint16    regI;
    uint8     regDT;
    uint8     regST;
    CallStack callStack;
    InstructionPointer instructionPointer;
    Memory    memory;
    Display   display;
    Rand      rand;
    
    public Cpu (Memory memory, Display display) 
    {
        rand = new Rand.with_seed(564897);
        regV = new uint8[16];
        this.memory = memory;
        this.display = display;
        callStack = new CallStack(16);
        instructionPointer = new InstructionPointer(0x200);
    }
    
    public void cycle () throws CallStackError
    {
       if (regDT > 0)
           --regDT;
       
       if (regST > 0)
           --regST;
       
       uint16 instruction = memory.getInstruction(instructionPointer.next());
       
       dispatch(instruction);
    }
    
    void dispatch (uint16 instruction) throws CallStackError
    {
            uint16 op    = instruction >> 12;
            uint16 args  = instruction & 0xfff;
            uint8  arg1  = (uint8)((instruction >> 8) & 0xF);
            uint8  arg2  = (uint8)((instruction >> 4) & 0xF);
            uint8  arg3  = (uint8)(instruction & 0xF);
            uint8  arg23 = (uint8)(instruction & 0xFF);
            
            stdout.printf("[%04x] %s %u\n", 
                          instructionPointer.peek()-2, 
                          insToString(instruction), regV[0x2]);

            switch (op) {
                case 0x0:
                    if (0x0 == arg1 && 0xE == arg2) {
                        if (0xE == arg3)
                            instructionPointer.ret();                            
                        else if(0x0 == arg3)
                            display.clear();
                    } else {
                        // SYS
                    }
                    break;
                case 0x1:
                    instructionPointer.jumpTo(args);
                    break;
                case 0x2:
                    instructionPointer.call(args);
                    break;
                case 0x3:
                    if (regV[arg1] == arg23)
                        instructionPointer.skip();
                    break;
                case 0x4:
                    if (regV[arg1] != arg23)
                        instructionPointer.skip();
                    break;
                case 0x5:
                    if (0x0 == arg3 && regV[arg1] == regV[arg2])
                        instructionPointer.skip();
                    break;
                case 0x6:
                    regV[arg1] = arg23;
                    break;
                case 0x7:
                    regV[arg1] += arg23;
                    break;
                case 0x8:
                    switch (arg3) {
                        case 0x0:
                            regV[arg1] = regV[arg2];
                            break;
                        case 0x1:
                            regV[arg1] |= regV[arg2];
                            break;
                        case 0x2:
                            regV[arg1] &= regV[arg2];
                            break;
                        case 0x3:
                            regV[arg1] ^= regV[arg2];
                            break;
                        case 0x4:
                            uint8 t = regV[arg1];
                            regV[arg1] += regV[arg2];
                            regV[0xF] = (uint8)(t > regV[arg1]);
                            break;
                        case 0x5:
                            uint8 t = regV[arg1];
                            regV[arg1] -= regV[arg2];
                            regV[0xF] = (uint8)(t > regV[arg1]);
                            break;
                        case 0x6:
                            regV[0xF] = regV[arg1] & 1;
                            regV[arg1] >>= 1;
                            break;
                        case 0x7:
                            uint8 t = regV[arg2];
                            regV[arg1] = regV[arg2] - regV[arg1];
                            regV[0xF] = (uint8)(t > regV[arg2]);
                            break;
                        case 0xE:
                            regV[0xF] = regV[arg1] >> 7;
                            regV[arg1] <<= 1;
                            break;
                    }
                    break;
                case 0x9:
                    if (0x0 == arg3 && regV[arg1] != regV[arg2])
                        instructionPointer.skip();
                    break;
                case 0xA:
                    regI = args;
                    break;
                case 0xB:
                    instructionPointer.jumpTo(args + regV[0]);
                    break;
                case 0xC:
                    regV[arg1] = (uint8)rand.int_range(0, 0xFFFF) & arg23;
                    break;
                case 0xD:
                    regV[0xF] = display.draw(memory.bytes[regI:regI + arg3], 
                                             regV[arg1], 
                                             regV[arg2]);
                    break;
                case 0xE:
                    if (0x9E == arg23 && getKey(regV[arg1]))
                        instructionPointer.skip();
                    else if(0xA1 == arg23 && !getKey(regV[arg1]))
                        instructionPointer.skip();
                    break;
                case 0xF:
                    switch (arg23) {
                        case 0x07:
                            regV[arg1] = regDT;
                            break;
                        case 0x0A:
                            regV[arg1] = waitForAnyKeys();
                            break;
                        case 0x15:
                            regDT = regV[arg1];
                            break;
                        case 0x18:
                            regST = regV[arg1];
                            break;
                        case 0x1E:
                            regI += regV[arg1];
                            break;
                        case 0x29:
                            regI = regV[arg1] * 5;
                            break;
                        case 0x33:
                            memory.bytes[regI]     =  regV[arg1] / 100;
                            memory.bytes[regI + 1] = (regV[arg1] / 10) % 10;
                            memory.bytes[regI + 2] = regV[arg1] % 10;
                            break;
                        case 0x55:
                            for (int i = 0; i <= arg1; ++i)
                                memory.bytes[regI++] = regV[i];
                            break;
                        case 0x65:
                            for (int i = 0; i <= arg1; ++i)
                                regV[i] = memory.bytes[regI++];
                            break;
                    }
                    break;
        }
    }
}

public errordomain CallStackError {
        EMPTY,
        FULL
}

class InstructionPointer
{
    uint16 pointer;
    CallStack callStack;
    
    public InstructionPointer (uint16 start)
    {
        pointer = start;
        callStack = new CallStack(16);
    }
    
    
    public uint16 peek()
    {
        return pointer;
    }
    
    
    public uint16 next()
    {
        uint16 cur = pointer;
        pointer += 2;
        return cur;
    }
    
    public void skip()
    {
        pointer += 2;
    }
    
    public void jumpTo(uint16 pointer)
    {
        this.pointer = pointer;
    }
    
    public void call(uint16 pointer) throws CallStackError
    {
        callStack.push(this.pointer);
        this.pointer = pointer;
        
    }
    
    public void ret() throws CallStackError
    {
        pointer = callStack.pop();
    }
}

class CallStack
{
    
    uint16[] stack;
    size_t pointer;
    
    public CallStack (size_t length)
    {
        stack = new uint16[length];
    }
    
    public void push (uint16 element) throws CallStackError
    {
        if (pointer == stack.length) {
            printStack();
            throw new CallStackError.FULL("Callstack is full.");
            
        }
            
        stack[pointer++] = element;
    }
    
    public uint16 pop () throws CallStackError
    {
        if (pointer == 0)
            throw new CallStackError.EMPTY("Callstack is empty.");
            
        return stack[--pointer];
    }
    
    void printStack ()
    {
        stdout.printf("pointer: %zu\n", pointer);
        
        foreach (uint16 el in stack)
            stdout.printf("%u\n", el);
    }
}